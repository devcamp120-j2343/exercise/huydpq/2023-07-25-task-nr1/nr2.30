const express = require("express");
const mongoose = require("mongoose");
// Khai báo Monolithic
const path = require('path')
const port = 8000;
const app = express();

app.use(express.json());

// khai báo model
 const userModel = require("./app/model/userModel");
 const diceHistory = require("./app/model/diceHistoryModel");
 const prizeModel = require("./app/model/prizeModel");
 const voucherModel = require("./app/model/voucherModel");
 const prizeHistoryModel = require("./app/model/prizeHistoryModel");
 const voucherHistoryModel = require("./app/model/voucherHistoryModel");



 // khai báo Router
const { userRouter } = require("./app/router/userRouter");
const { diceHistoryRouter } = require("./app/router/diceHistoryRouter");
const { prizeRouter } = require("./app/router/prizeRouter");
const { voucherRouter } = require("./app/router/voucherRouter");
const { prizeHistoryRouter } = require("./app/router/prizeHistoryRouter");
const { voucherHistoryRouter } = require("./app/router/voucherHistoryRouter");

mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Lucky_Dice_Casino")
.then(() => console.log("Connected to Mongo Successfully"))
.catch(error => handleError(error));

app.use(express.static(__dirname + "/view/LuckyDice"))
app.get('/', (req, res) => { 
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/view/LuckyDice/Task 23 Lucky Dice.html"))
})

app.use("/user", userRouter)
app.use("/dice-histories", diceHistoryRouter)
app.use("/prize", prizeRouter)
app.use("/voucher", voucherRouter)
app.use("/prize-histories", prizeHistoryRouter)
app.use("/voucher-histories", voucherHistoryRouter)


app.listen(port, () => {
    console.log(`Đang chạy trên cổng ${port}`)
})