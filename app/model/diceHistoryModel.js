const mongoose = require("mongoose");

const schema = mongoose.Schema;

const diceHistorySchema = new schema({
    _id: mongoose.Types.ObjectId,
    user: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: "user"
    },
    dice: {
        type: Number,
        required: true
    }  
},{
    timestamps: true
}
)

module.exports = mongoose.model("diceHistory", diceHistorySchema)