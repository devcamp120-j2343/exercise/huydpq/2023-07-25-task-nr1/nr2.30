const express = require("express");
const { createPrize, getAllPrize, getPrizeById, updatePrize, deletePrize, } = require("../controller/prizeController");

const prizeRouter = express.Router();

prizeRouter.post("/",createPrize)

prizeRouter.get("/",getAllPrize)

prizeRouter.get("/:prizeId",getPrizeById)

prizeRouter.put("/:prizeId",updatePrize)

prizeRouter.delete("/:prizeId", deletePrize)



module.exports = {prizeRouter}