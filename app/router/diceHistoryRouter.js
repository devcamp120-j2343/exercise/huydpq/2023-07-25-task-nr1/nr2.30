const express = require("express");
const { createDiceHistory, getAllDiceHistory, updateDiceHistory, deleteDiceHistory } = require("../controller/diceHistoryController");

const diceHistoryRouter = express.Router();

diceHistoryRouter.post("/", createDiceHistory)

diceHistoryRouter.get("/", getAllDiceHistory)

diceHistoryRouter.get("/:diceId", getAllDiceHistory)

diceHistoryRouter.put("/:diceId", updateDiceHistory)

diceHistoryRouter.delete("/:diceId", deleteDiceHistory)

module.exports = {diceHistoryRouter}