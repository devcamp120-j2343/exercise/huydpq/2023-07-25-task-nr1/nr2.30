const express = require("express");
const { createPrizeHistory, getAllPrizeHistory, getPrizeHistoryByID, updatePrizeHistory, deletePrizeHistory } = require("../controller/prizeHistoryController");

const prizeHistoryRouter = express.Router();

prizeHistoryRouter.post("/", createPrizeHistory)

prizeHistoryRouter.get("/", getAllPrizeHistory)

prizeHistoryRouter.get("/:prizeHistoryId", getPrizeHistoryByID)

prizeHistoryRouter.put("/:prizeHistoryId", updatePrizeHistory)

prizeHistoryRouter.delete("/:prizeHistoryId", deletePrizeHistory)

module.exports = {prizeHistoryRouter}