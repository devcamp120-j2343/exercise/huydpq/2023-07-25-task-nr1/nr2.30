const express = require("express");
const { createVoucherHistory, getAllVoucherHistory, getVoucherHistoryByID, updateVoucherHistory, deleteVoucherHistory } = require("../controller/voucherHistoryController");

const voucherHistoryRouter = express.Router();

voucherHistoryRouter.post("/", createVoucherHistory)

voucherHistoryRouter.get("/", getAllVoucherHistory)

voucherHistoryRouter.get("/:voucherHistoryId", getVoucherHistoryByID)

voucherHistoryRouter.put("/:voucherHistoryId", updateVoucherHistory)

voucherHistoryRouter.delete("/:voucherHistoryId", deleteVoucherHistory)



module.exports = {voucherHistoryRouter}