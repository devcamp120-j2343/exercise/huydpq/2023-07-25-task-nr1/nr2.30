const { default: mongoose } = require("mongoose")
const prizeModel = require("../model/prizeModel")

// creare
const createPrize = async (req, res) => {
    const { _id, name, description } = req.body

    let newPrize = {
        _id: new mongoose.Types.ObjectId(),
        name,
        description
    }
    try {
        const create = await prizeModel.create(newPrize)
        return res.status(201).json({
            status: "Create Prize successfully",
            create
        })

    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}
// get all
const getAllPrize = async (req, res) => {
    try {
        const getAll = await prizeModel.find()
        return res.status(200).json({
            status: "Get All Prize successfully",
            data: getAll
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

// get all by Id
const getPrizeById = async (req, res) => {
    let id = req.params.prizeId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: `prizeId is invalid`
        })
    }
    try {
        const getByid = await prizeModel.findById(id)
        if (getByid) {
            return res.status(200).json({
                status: "Get All Prize successfully",
                data: getByid
            })
        } else {
            return res.status(404).json({
                status: "Not found any review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}
// update 
const updatePrize = async (req, res) => {
    let id = req.params.prizeId;
    let { name, description } = req.body

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: `prizeId is invalid`
        })
    }
    try {
        let updatePrize = {
            name,
            description
        }
        const update = await prizeModel.findByIdAndUpdate(id, updatePrize)
        if (update) {
            return res.status(200).json({
                status: "Update Prize successfully",
                data: update
            })
        } else {
            return res.status(404).json({
                status: "Not found any review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}
// delete
const deletePrize = async (req, res) => {
    let id = req.params.prizeId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: `prizeId is invalid`
        })
    }
    try {
        const detete = await prizeModel.findByIdAndDelete(id)
        if (detete) {
            return res.status(200).json({
                status: "Get All Prize successfully",
                data: detete
            })
        } else {
            return res.status(404).json({
                status: "Not found any review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}


module.exports = { createPrize, getAllPrize, getPrizeById, updatePrize, deletePrize }