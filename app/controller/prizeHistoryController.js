const { default: mongoose } = require("mongoose");
const prizeHistoryModel = require("../model/prizeHistoryModel");

const createPrizeHistory = async (req, res) => {
    const {user, prize} = req.body;

    if(!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: "user is invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(prize)) {
        return res.status(400).json({
            status: "prize is invalid"
        })
    }
    try {
        let newPrizeHistory = {
            _id: new mongoose.Types.ObjectId(),
            user,
            prize
        }
        const create = await prizeHistoryModel.create(newPrizeHistory)
        return  res.status(200).json({
            status: "Create Prize History successfully",
            data: create
        })

    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

const getAllPrizeHistory = async (req, res) => {
    let userId = req.query.userId;
    let prizeId = req.query.prizeId;

    let condition = {};
    if(userId) {
        condition.user = userId
    }
    if(prizeId) {
        condition.prize = prizeId
    }
    console.log(condition)
    try {
        const getAll = await prizeHistoryModel.find(condition)

        return  res.status(200).json({
            status: "Get All Prize History successfully",
            data: getAll
        })
        
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

const getPrizeHistoryByID = async (req, res) => {
    let id = req.params.prizeHistoryId;

    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "id is invalid"
        })
    }

    try {
        const getById = await prizeHistoryModel.findById(id);
        if(getById){
            return  res.status(200).json({
                status: "Get Prize History By Id successfully",
                data: getById
            })
        } else {
            return res.status(404).json({
                status: "Not Found Any Voucher",
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}
const updatePrizeHistory = async (req, res) => {
    let id = req.params.prizeHistoryId;
    const {user, prize} = req.body;

    if(!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: "user is invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(prize)) {
        return res.status(400).json({
            status: "prize is invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "id is invalid"
        })
    }
    try {
        let updatePrize = {
            user,
            prize
        }
        const update = await prizeHistoryModel.findByIdAndUpdate(id, updatePrize);
        if(update){
            return  res.status(200).json({
                status: "Update Prize History  successfully",
                data: update
            })
        } else {
            return res.status(404).json({
                status: "Not Found Any Voucher",
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

const deletePrizeHistory = async (req, res) => {
    let id = req.params.prizeHistoryId;
    
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "id is invalid"
        })
    }
    try {
        const deletePrize = await prizeHistoryModel.findByIdAndDelete(id);
        if(deletePrize){
            return  res.status(200).json({
                status: "Delete Prize History By Id successfully",
                data: deletePrize
            })
        } else {
            return res.status(404).json({
                status: "Not Found Any Voucher",
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

module.exports = {createPrizeHistory, getAllPrizeHistory, getPrizeHistoryByID, updatePrizeHistory, deletePrizeHistory}