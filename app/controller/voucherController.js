const { default: mongoose } = require("mongoose");
const voucherModel = require("../model/voucherModel");
const randtoken = require('rand-token');

const createVoucher = async (req, res) => {
    const { code, discount, note } = req.body;
    try {
        let newVoucher = {
            _id: new mongoose.Types.ObjectId(),
            code: randtoken.generate(10),
            discount,
            note
        }
        const create = await voucherModel.create(newVoucher);
        return res.status(200).json({
            status: "Create Voucher successfully",
            data: create
        })

    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

const getAllVoucher = async (req, res) => {
    try {
        const getAll = await voucherModel.find()
        return res.status(200).json({
            status: "Get All Voucher successfully",
            data: getAll
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

const getVoucherById = async (req, res) => {
    let id = req.params.voucherId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: `voucherId is invalid`
        })
    }
    try {
        const getById = await voucherModel.findById(id)
        if(getById){
            return res.status(200).json({
                status: "Get Voucher By ID successfully",
                data: getById
            })
        } else {
            return res.status(404).json({
                status: "Not Found Any Voucher",
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

const updateVoucher = async (req, res) => {
    let id = req.params.voucherId;
    const { discount, note } = req.body;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: `voucherId is invalid`
        })
    }
    try {
        let update = {
            discount,
            note
        }
        const updateVoucher = await voucherModel.findByIdAndUpdate(id, update )
        if(updateVoucher){
            return res.status(200).json({
                status: "Create Voucher successfully",
                data: updateVoucher
            })
        } else {
            return res.status(404).json({
                status: "Not Found Any Voucher",
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}
const deleteVoucher = async (req, res) => {
    let id = req.params.voucherId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: `voucherId is invalid`
        })
    }
    try {
        const deleteVoucher = await voucherModel.findByIdAndDelete(id)
        if(deleteVoucher){
            return res.status(200).json({
                status: "Delete Voucher successfully",
                data: deleteVoucher
            })
        } else {
            return res.status(404).json({
                status: "Not Found Any Voucher",
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}


module.exports = { createVoucher, getAllVoucher, getVoucherById, updateVoucher, deleteVoucher }