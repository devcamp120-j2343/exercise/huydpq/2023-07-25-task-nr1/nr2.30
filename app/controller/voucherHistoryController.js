const { default: mongoose } = require("mongoose");
const voucherHistoryModel = require("../model/voucherHistoryModel");

const createVoucherHistory = async (req, res) => {
    const {user, voucher} = req.body;

    if(!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: "user is invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            status: "voucher is invalid"
        })
    }
    try {
        let newVoucherHistory = {
            _id: new mongoose.Types.ObjectId(),
            user,
            voucher
        }
        const create = await voucherHistoryModel.create(newVoucherHistory)
        return  res.status(200).json({
            status: "Create Voucher History successfully",
            data: create
        })

    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}
const getAllVoucherHistory = async (req, res) => {
    let userId = req.query.userId;
    let voucherId = req.query.voucherId;

    let condition = {};
    if(userId) {
        condition.user = userId
    }
    if(voucherId) {
        condition.voucher = voucherId
    }
    console.log(condition)
    try {
        const getAll = await voucherHistoryModel.find(condition)

        return  res.status(200).json({
            status: "Get All Voucher History successfully",
            data: getAll
        })
        
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}
const getVoucherHistoryByID = async (req, res) => {
    let id = req.params.voucherHistoryId;

    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "id is invalid"
        })
    }

    try {
        const getById = await voucherHistoryModel.findById(id);
        if(getById){
            return  res.status(200).json({
                status: "Get Voucher History By Id successfully",
                data: getById
            })
        } else {
            return res.status(404).json({
                status: "Not Found Any Voucher",
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

const updateVoucherHistory = async (req, res) => {
    let id = req.params.voucherHistoryId;
    const {user, voucher} = req.body;

    if(!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: "user is invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            status: "prize is invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "id is invalid"
        })
    }
    try {
        let updateVoucher = {
            user,
            voucher
        }
        const update = await voucherHistoryModel.findByIdAndUpdate(id, updateVoucher);
        if(update){
            return  res.status(200).json({
                status: "Update Voucher History successfully",
                data: update
            })
        } else {
            return res.status(404).json({
                status: "Not Found Any Voucher",
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}
const deleteVoucherHistory = async (req, res) => {
    let id = req.params.voucherHistoryId;
    
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "id is invalid"
        })
    }
    try {
        const deleteVoucher = await voucherHistoryModel.findByIdAndDelete(id);
        if(deleteVoucher){
            return  res.status(200).json({
                status: "Delete Voucher History By Id successfully",
                data: deleteVoucher
            })
        } else {
            return res.status(404).json({
                status: "Not Found Any Voucher",
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

module.exports = {createVoucherHistory, getAllVoucherHistory, getVoucherHistoryByID, updateVoucherHistory, deleteVoucherHistory}